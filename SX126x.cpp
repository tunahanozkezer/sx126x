/*
 * SX126x.cpp
 *
 *  Created on: April 1, 2020
 *      Author: Tunahan ÖZKEZER
 */

#include "SX126x.h"



SX126x::SX126x()
{
	// TODO Auto-generated constructor stub
}


uint8_t SX126x::busy_Pin()
{
	uint8_t busyState=HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_12);
	return busyState;
}

void SX126x::cs_Pin(uint8_t a){
	if(a>=1) {HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);}
    else     {HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);}
	}

void SX126x::reset_Pin(uint8_t a){
	if(a>=1) {HAL_GPIO_WritePin(rstPin_GPIO_Port, rstPin_Pin, GPIO_PIN_SET);}
    else     {HAL_GPIO_WritePin(rstPin_GPIO_Port, rstPin_Pin, GPIO_PIN_RESET);}
	}

uint8_t SX126x::int_Pin()
{
	uint8_t intState=HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_14);
	return  intState;
}



int16_t SX126x::init(SPI_HandleTypeDef *hspi1, uint8_t txPowerInDbm, uint32_t frequencyInHz)
{
	cs_Pin(1);
	spiI = hspi1;
  if ( txPowerInDbm > 22 )
    txPowerInDbm = 22;
  if ( txPowerInDbm < -3 )
    txPowerInDbm = -3;
  
  Reset();
  

  SetStandby(SX126X_STANDBY_RC);
  
  SetDio3AsTcxoCtrl(SX126X_DIO3_OUTPUT_3_3, RADIO_TCXO_SETUP_TIME << 6); // convert from ms to SX126x time base
  
  Calibrate(  SX126X_CALIBRATE_IMAGE_ON
                  | SX126X_CALIBRATE_ADC_BULK_P_ON
                  | SX126X_CALIBRATE_ADC_BULK_N_ON
                  | SX126X_CALIBRATE_ADC_PULSE_ON
                  | SX126X_CALIBRATE_PLL_ON
                  | SX126X_CALIBRATE_RC13M_ON
                  | SX126X_CALIBRATE_RC64K_ON
                  );

  SetDio2AsRfSwitchCtrl(true);

  SetStandby(SX126X_STANDBY_RC); 
  SetRegulatorMode(SX126X_REGULATOR_DC_DC);
  SetBufferBaseAddress(0, 0);
  SetPaConfig(0x04, 0x07, 0x00, 0x01);
  SetOvercurrentProtection(0x38);  // current max 30mA for the whole device
  SetPowerConfig(txPowerInDbm, SX126X_PA_RAMP_200U); //0 fuer Empfaenger
  SetDioIrqParams(SX126X_IRQ_ALL,  //all interrupts enabled
                  (SX126X_IRQ_RX_DONE | SX126X_IRQ_TX_DONE | SX126X_IRQ_TIMEOUT), //interrupts on DIO1
                  SX126X_IRQ_NONE,  //interrupts on DIO2
                  SX126X_IRQ_NONE); //interrupts on DIO3

  SetRfFrequency(frequencyInHz);
  if ( 0x2A != GetStatus() )
   {
     //Serial.println("SX126x error, maybe no SPI connection?");
     return GetStatus();
   }
  else
  {
	 return 1;
  }

}


int16_t SX126x::LoRaConfig(uint8_t spreadingFactor, uint8_t bandwidth, uint8_t codingRate, uint16_t preambleLength, uint8_t payloadLen, bool crcOn, bool invertIrq) 
{
	cs_Pin(1);
  uint8_t ldro; //LowDataRateOptimize

  SetStopRxTimerOnPreambleDetect(false);
  SetLoRaSymbNumTimeout(0); 
  SetPacketType(SX126X_PACKET_TYPE_LORA); //RadioSetModem( ( SX126x.ModulationParams.PacketType == PACKET_TYPE_GFSK ) ? MODEM_FSK : MODEM_LORA );
  SetModulationParams(spreadingFactor, bandwidth, codingRate, ldro);
  
  PacketParams[0] = (preambleLength >> 8) & 0xFF;
  PacketParams[1] = preambleLength;
  if ( payloadLen )
  {
    //fixed payload length
    PacketParams[2] = 0x01;
    PacketParams[3] = payloadLen;
  }
  else
  {
    PacketParams[2] = 0x00;
    PacketParams[3] = 0xFF;
  }

  if ( crcOn )
    PacketParams[4] = 0x01;
  else
    PacketParams[4] = 0x00;

  if ( invertIrq )
    PacketParams[5] = 0x01;
  else
    PacketParams[5] = 0x00;

  //SPIwriteCommand(SX126X_CMD_SET_PACKET_PARAMS, PacketParams, 6);

  uint8_t cmd_set = SX126X_CMD_SET_PACKET_PARAMS;
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, &cmd_set, 1);
  HAL_SPI_Transmit_IT(spiI, PacketParams, 6);
  cs_Pin(1);
  SetDioIrqParams(SX126X_IRQ_ALL,  //all interrupts enabled
                  (SX126X_IRQ_RX_DONE | SX126X_IRQ_TX_DONE, SX126X_IRQ_TIMEOUT), //interrupts on DIO1
                  SX126X_IRQ_NONE,  //interrupts on DIO2
                  SX126X_IRQ_NONE);

  SetRx(0xFFFFFF);
}


uint8_t SX126x::Receive(uint8_t *pData, uint16_t len) 
{
  uint8_t rxLen = 0;
  uint16_t irqRegs = GetIrqStatus();
  
  if( irqRegs & SX126X_IRQ_RX_DONE )
  {
    ClearIrqStatus(SX126X_IRQ_RX_DONE);
    ReadBuffer(pData, &rxLen, len);
  }
  
  return rxLen;
}


bool SX126x::Send(uint8_t *pData, uint8_t len, uint8_t mode)
{
  uint16_t irq;
  bool rv = false;
  
  if ( txActive == false )
  {
	  txActive = true;
	  PacketParams[2] = 0x00; //Variable length packet (explicit header)
	  PacketParams[3] = len;
	  uint8_t cmd_set=SX126X_CMD_SET_PACKET_PARAMS;

	  cs_Pin(0);
	  HAL_SPI_Transmit_IT(spiI, &cmd_set, 1);
	  HAL_SPI_Transmit_IT(spiI, PacketParams, 6);
	  cs_Pin(1);

	  //SPIwriteCommand(SX126X_CMD_SET_PACKET_PARAMS, PacketParams, 6);
	  
	  ClearIrqStatus(SX126X_IRQ_TX_DONE | SX126X_IRQ_TIMEOUT);

	  WriteBuffer(pData, len);
	  SetTx(500);
	  
	  if ( mode & SX126x_TXMODE_SYNC )
	  {

	    irq = GetIrqStatus();
	    while ( (!(irq & SX126X_IRQ_TX_DONE)) && (!(irq & SX126X_IRQ_TIMEOUT)) )
	    {
	      irq = GetIrqStatus();
	    }
	    txActive = false;
	
	    SetRx(0xFFFFFF);
	
	    if ( irq != SX126X_IRQ_TIMEOUT )
	    	rv = true;
	  }
	  else
	  {
	    rv = true;
	  }
	}
	
	return rv;
}


bool SX126x::ReceiveMode(void)
{
  uint16_t irq;
  bool rv = false;

  if ( txActive == false )
  {
    rv = true;
  }
  else
  {
    irq = GetIrqStatus();
    if ( irq & (SX126X_IRQ_TX_DONE | SX126X_IRQ_TIMEOUT) )
    { 
      SetRx(0xFFFFFF);
      txActive = false;
      rv = true;
    }
  }

  return rv;
}


void SX126x::ReceiveStatus(uint8_t *rssiPacket, uint8_t *snrPacket)
{
    uint8_t buf[3];
    uint8_t read[4];
    buf[0]=SX126X_CMD_GET_PACKET_STATUS;
    buf[1]=SX126X_CMD_NOP;
    HAL_SPI_Transmit_IT(spiI, buf, 2);
    HAL_SPI_Receive_IT(spiI, read, 3);
     
    /*
    SPIreadCommand( SX126X_CMD_GET_PACKET_STATUS, buf, 3 );

    ( buf[1] < 128 ) ? ( *snrPacket = buf[1] >> 2 ) : ( *snrPacket = ( ( buf[1] - 256 ) >> 2 ) );
    *rssiPacket = -buf[0] >> 1;
    *rssiPacket
    *rssiPacket */
}


void SX126x::SetTxPower(int8_t txPowerInDbm)
{
  SetPowerConfig(txPowerInDbm, SX126X_PA_RAMP_200U);
}


void SX126x::Reset(void)
{
	HAL_Delay(10);
	reset_Pin(0);
	HAL_Delay(20);
	reset_Pin(1);
	HAL_Delay(10);
	while(busy_Pin());
}


void SX126x::Wakeup(void)
{
  GetStatus();
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command SetStandby(...) is used to set the device in a configuration mode which is at an intermediate level of
//  consumption. In this mode, the chip is placed in halt mode waiting for instructions via SPI. This mode is dedicated to chip
//  configuration using high level commands such as SetPacketType(...).
//  By default, after battery insertion or reset operation (pin NRESET goes low), the chip will enter in STDBY_RC mode running
//  with a 13 MHz RC clock
//
//  Parameters
//  ----------
//  0: Device running on RC13M, set STDBY_RC mode
//  1: Device running on XTAL 32MHz, set STDBY_XOSC mode
//
//  Return value
//  ------------
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetStandby(uint8_t mode)
{
  uint8_t buf[2];
  buf[0]=SX126X_CMD_SET_STANDBY;
  buf[1]=mode;
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, buf, 2);
  cs_Pin(1);
//  SPIwriteCommand(SX126X_CMD_SET_STANDBY, &data, 1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The host can retrieve chip status directly through the command GetStatus() : this command can be issued at any time and
//  the device returns the status of the device. The command GetStatus() is not strictly necessary since device returns status
//  information also on command bytes.
//
//  Parameters:
//  none
//
//  Return value:
//  Bit 6:4 Chipmode:0x0: Unused
//  Bit 3:1 Command Status
//  Bit 0: unused
//  Bit 7: unused
//----------------------------------------------------------------------------------------------------------------------------
uint8_t SX126x::GetStatus(void)
{
  uint8_t rv;
  uint8_t get_status = SX126X_CMD_GET_STATUS;
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, &get_status, 1);
  HAL_SPI_Receive_IT(spiI, &rv, 1);
  cs_Pin(1);
  //SPIreadCommand(SX126X_CMD_GET_STATUS, &rv, 1);
  return rv;
}


//----------------------------------------------------------------------------------------------------------------------------
//  The BUSY line is mandatory to ensure the host controller is ready to accept SPI commands.
//  When BUSY is high, the host controller must wait until it goes down again before sending another command.
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::WaitOnBusy( void )
{
  while(busy_Pin());
}



//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetDio3AsTcxoCtrl(uint8_t tcxoVoltage, uint32_t timeout)
{
    uint8_t buf[5];
    buf[0] = SX126X_CMD_SET_DIO3_AS_TCXO_CTRL;
    buf[1] = tcxoVoltage & 0x07;
    buf[2] = ( uint8_t )( ( timeout >> 16 ) & 0xFF );
    buf[3] = ( uint8_t )( ( timeout >> 8 ) & 0xFF );
    buf[4] = ( uint8_t )( timeout & 0xFF );
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 5);
    cs_Pin(1);
//    SPIwriteCommand(SX126X_CMD_SET_DIO3_AS_TCXO_CTRL, buf, 4);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::Calibrate(uint8_t calibParam)
{
	uint8_t buf[2];
	buf[0]=SX126X_CMD_CALIBRATE;
	buf[1]=calibParam;
	cs_Pin(0);
	HAL_SPI_Transmit_IT(spiI, buf, 2);
	cs_Pin(1);
//  SPIwriteCommand(SX126X_CMD_CALIBRATE, &data, 1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetDio2AsRfSwitchCtrl(uint8_t enable)
{
		uint8_t buf[2];
		buf[0]=SX126X_CMD_SET_DIO2_AS_RF_SWITCH_CTRL;
		buf[1]=enable;
		cs_Pin(1);
		HAL_SPI_Transmit_IT(spiI, buf, 2);
		cs_Pin(0);
  //SPIwriteCommand(SX126X_CMD_SET_DIO2_AS_RF_SWITCH_CTRL, &data, 1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetRfFrequency(uint32_t frequency)
{
  uint8_t buf[5];
  uint32_t freq = 0;

  CalibrateImage(frequency);

  freq = (uint32_t)((double)frequency / (double)FREQ_STEP);
  buf[0]=SX126X_CMD_SET_RF_FREQUENCY;
  buf[1] = (uint8_t)((freq >> 24) & 0xFF);
  buf[2] = (uint8_t)((freq >> 16) & 0xFF);
  buf[3] = (uint8_t)((freq >> 8) & 0xFF);
  buf[4] = (uint8_t)(freq & 0xFF);
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, buf, 5);
  cs_Pin(1);
  //SPIwriteCommand(SX126X_CMD_SET_RF_FREQUENCY, buf, 4);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::CalibrateImage(uint32_t frequency)
{
  uint8_t calFreq[3];
  calFreq[0]=SX126X_CMD_CALIBRATE_IMAGE;
  if( frequency> 900000000 )
  {
      calFreq[1] = 0xE1;
      calFreq[2] = 0xE9;
  }
  else if( frequency > 850000000 )
  {
      calFreq[1] = 0xD7;
      calFreq[2] = 0xD8;
  }
  else if( frequency > 770000000 )
  {
      calFreq[1] = 0xC1;
      calFreq[2] = 0xC5;
  }
  else if( frequency > 460000000 )
  {
      calFreq[1] = 0x75;
      calFreq[2] = 0x81;
  }
  else if( frequency > 425000000 )
  {
      calFreq[1] = 0x6B;
      calFreq[2] = 0x6F;
  }
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, calFreq, 3);
  cs_Pin(1);
  //SPIwriteCommand(SX126X_CMD_CALIBRATE_IMAGE, calFreq, 2);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetRegulatorMode(uint8_t mode)
{
    uint8_t buf[2];
    		buf[0]=SX126X_CMD_SET_DIO2_AS_RF_SWITCH_CTRL;
    		buf[1]=mode;
    		cs_Pin(0);
      HAL_SPI_Transmit_IT(spiI, buf, 2);
      cs_Pin(1);
    //SPIwriteCommand(SX126X_CMD_SET_REGULATOR_MODE, &data, 1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetBufferBaseAddress(uint8_t txBaseAddress, uint8_t rxBaseAddress)
{
    uint8_t buf[3];

    buf[0] = SX126X_CMD_SET_BUFFER_BASE_ADDRESS;
    buf[1] = txBaseAddress;
    buf[2] = rxBaseAddress;
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 3);
    cs_Pin(1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetPowerConfig(int8_t power, uint8_t rampTime)
{
    uint8_t buf[3];

    if( power > 22 )
    {
        power = 22;
    }
    else if( power < -3 )
    {
        power = -3;
    }
    buf[0] = SX126X_CMD_SET_TX_PARAMS;
    buf[1] = power;
    buf[2] = ( uint8_t )rampTime;
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 3);
    cs_Pin(1);
    //SPIwriteCommand(SX126X_CMD_SET_TX_PARAMS, buf, 2);

}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetPaConfig(uint8_t paDutyCycle, uint8_t hpMax, uint8_t deviceSel, uint8_t paLut)
{
    uint8_t buf[5];

    buf[0] = SX126X_CMD_SET_PA_CONFIG;
    buf[1] = paDutyCycle;
    buf[2] = hpMax;
    buf[3] = deviceSel;
    buf[4] = paLut;
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 5);
    cs_Pin(1);
    //SPIwriteCommand(SX126X_CMD_SET_PA_CONFIG, buf, 4);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The OCP is configurable by steps of 2.5 mA and the default value is re-configured automatically each time the function
//  SetPaConfig(...) is called. If the user wants to adjust the OCP value, it is necessary to change the register as a second 
//  step after calling the function SetPaConfig.
//
//  Parameters:
//  value: steps of 2,5mA (0x18 = 60mA)
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetOvercurrentProtection(uint8_t value)
{
  uint8_t buf[4];

  buf[0] = SX126X_CMD_WRITE_REGISTER;
  buf[1] = ((SX126X_REG_OCP_CONFIGURATION & 0xFF00) >> 8);
  buf[2] = (SX126X_REG_OCP_CONFIGURATION & 0x00FF);
  buf[3] = value;
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, buf, 4);
  cs_Pin(1);
  //SPIwriteCommand(SX126X_CMD_WRITE_REGISTER, buf,3);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetDioIrqParams( uint16_t irqMask, uint16_t dio1Mask, uint16_t dio2Mask, uint16_t dio3Mask )
{
    uint8_t buf[9];

    buf[0] = SX126X_CMD_SET_DIO_IRQ_PARAMS;
    buf[1] = (uint8_t)((irqMask >> 8) & 0x00FF);
    buf[2] = (uint8_t)(irqMask & 0x00FF);
    buf[3] = (uint8_t)((dio1Mask >> 8) & 0x00FF);
    buf[4] = (uint8_t)(dio1Mask & 0x00FF);
    buf[5] = (uint8_t)((dio2Mask >> 8) & 0x00FF);
    buf[6] = (uint8_t)(dio2Mask & 0x00FF);
    buf[7] = (uint8_t)((dio3Mask >> 8) & 0x00FF);
    buf[8] = (uint8_t)(dio3Mask & 0x00FF);
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 9);
    cs_Pin(1);
    //SPIwriteCommand(SX126X_CMD_SET_DIO_IRQ_PARAMS, buf, 8);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetStopRxTimerOnPreambleDetect( bool enable )
{
  uint8_t buff[2];
  buff[0] = SX126X_CMD_STOP_TIMER_ON_PREAMBLE;
  buff[1] = (uint8_t)enable;

  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, buff, 2);
  cs_Pin(1);
//  SPIwriteCommand(SX126X_CMD_STOP_TIMER_ON_PREAMBLE, &data, 1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  In LoRa mode, when going into Rx, the modem will lock as soon as a LoRa® symbol has been detected which may lead to
//  false detection. This phenomena is quite rare but nevertheless possible. To avoid this, the command
//  SetLoRaSymbNumTimeout can be used to define the number of symbols which will be used to validate the correct
//  reception of a packet.
//
//  Parameters:
//  0:      validate the reception as soon as a LoRa® Symbol has been detected
//  1..255: When SymbNum is different from 0, the modem will wait for a total of SymbNum LoRa® symbol to validate, or not, the
//          correct detection of a LoRa packet. If the various states of the demodulator are not locked at this moment, the radio will
//          generate the RxTimeout IRQ.
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetLoRaSymbNumTimeout(uint8_t SymbNum)
{
  uint8_t buf[2];
  buf[0] = SX126X_CMD_SET_LORA_SYMB_NUM_TIMEOUT;
  buf[1] = SymbNum;
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, buf, 2);
  cs_Pin(1);
  //SPIwriteCommand(SX126X_CMD_SET_LORA_SYMB_NUM_TIMEOUT, &data, 1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetPacketType(uint8_t packetType)
{
    uint8_t buf[2];
    buf[0] = SX126X_CMD_SET_PACKET_TYPE;
    buf[1] = packetType;

    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 2);
    cs_Pin(1);
    //SPIwriteCommand(SX126X_CMD_SET_PACKET_TYPE, &data, 1);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetModulationParams(uint8_t spreadingFactor, uint8_t bandwidth, uint8_t codingRate, uint8_t lowDataRateOptimize)
{
  uint8_t data[5];
  //currently only LoRa supported
  data[0] = SX126X_CMD_SET_MODULATION_PARAMS;
  data[1] = spreadingFactor;
  data[2] = bandwidth;
  data[3] = codingRate;
  data[4] = lowDataRateOptimize;
  cs_Pin(0);
  HAL_SPI_Transmit_IT(spiI, data, 5);
  cs_Pin(1);
  //SPIwriteCommand(SX126X_CMD_SET_MODULATION_PARAMS, data, 4);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
uint16_t SX126x::GetIrqStatus( void )
{
    uint8_t data[2];
    uint8_t read[2];
    data[0] = SX126X_CMD_GET_IRQ_STATUS;
    data[1] = SX126X_CMD_NOP;
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, data, 2);
    HAL_SPI_Receive_IT(spiI, read, 2);
    cs_Pin(1);
    return (read[0]<<8)|read[1];
    //SPIreadCommand(SX126X_CMD_GET_IRQ_STATUS, data, 2);
    //return (data[0] << 8) | data[1];
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::ClearIrqStatus(uint16_t irq)
{
    uint8_t buf[3];


    buf[0] = SX126X_CMD_CLEAR_IRQ_STATUS;
    buf[1] = (uint8_t)(((uint16_t)irq >> 8) & 0x00FF);
    buf[2] = (uint8_t)((uint16_t)irq & 0x00FF);
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 3);
    cs_Pin(1);
    //SPIwriteCommand(SX126X_CMD_CLEAR_IRQ_STATUS, buf, 2);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetRx(uint32_t timeout)
{
    uint8_t buf[4];

    buf[0] = SX126X_CMD_SET_RX;
    buf[1] = (uint8_t)((timeout >> 16) & 0xFF);
    buf[2] = (uint8_t)((timeout >> 8) & 0xFF);
    buf[3] = (uint8_t )(timeout & 0xFF);
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 4);
    cs_Pin(1);
    //SPIwriteCommand(SX126X_CMD_SET_RX, buf, 3);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command SetTx() sets the device in transmit mode. When the last bit of the packet has been sent, an IRQ TX_DONE
//  is generated. A TIMEOUT IRQ is triggered if the TX_DONE IRQ is not generated within the given timeout period.
//  The chip goes back to STBY_RC mode after a TIMEOUT IRQ or a TX_DONE IRQ.
//  he timeout duration can be computed with the formula: Timeout duration = Timeout * 15.625 μs
//
//  Parameters:
//  0: Timeout disable, Tx Single mode, the device will stay in TX Mode until the packet is transmitted
//  other: Timeout in milliseconds, timeout active, the device remains in TX mode. The maximum timeout is then 262 s.
//  
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::SetTx(uint32_t timeoutInMs)
{
    uint8_t buf[4];
    uint32_t tout = (uint32_t)(timeoutInMs * 0,015625);

    buf[0] = SX126X_CMD_SET_TX;
    buf[1] = (uint8_t)((tout >> 16) & 0xFF);
    buf[2] = (uint8_t)((tout >> 8) & 0xFF);
    buf[3] = (uint8_t )(tout & 0xFF);
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 4);
    cs_Pin(1);
    // SPIwriteCommand(SX126X_CMD_SET_TX, buf, 3);
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
void SX126x::GetRxBufferStatus(uint8_t *payloadLength, uint8_t *rxStartBufferPointer)
{
    uint8_t buf[2];
    uint8_t read[2];
    buf[0] = SX126X_CMD_GET_RX_BUFFER_STATUS;
    buf[1] = SX126X_CMD_NOP;
    cs_Pin(0);
    HAL_SPI_Transmit_IT(spiI, buf, 2);
    HAL_SPI_Receive_IT(spiI, read, 2);
    cs_Pin(1);
    *payloadLength = read[0];
    *rxStartBufferPointer = read[1];
}


//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
uint8_t SX126x::ReadBuffer(uint8_t *rxData, uint8_t *rxDataLen,  uint8_t maxLen)
{
    uint8_t offset = 0;
    
    GetRxBufferStatus(rxDataLen, &offset);
    if( *rxDataLen > maxLen )
    {
        return 1;
    }
    
    while(busy_Pin());
    
    cs_Pin(0);
    uint8_t spiBuff[3];
    spiBuff[0]=SX126X_CMD_READ_BUFFER;
    spiBuff[1]=offset;
    spiBuff[2]=SX126X_CMD_NOP;
    HAL_SPI_Transmit_IT(spiI, spiBuff, 3);

    HAL_SPI_Transmit_IT(spiI, SX126X_CMD_NOP, 1);
    HAL_SPI_Receive_IT(spiI, rxData, *rxDataLen);

    cs_Pin(1);
    while(busy_Pin());
 
    return 0;
}

uint8_t SX126x::SPITransmit(uint8_t *data, uint8_t txDataLen)
{
	cs_Pin(0);
	HAL_SPI_Transmit_IT(spiI, data, txDataLen);
	cs_Pin(1);
}

uint8_t SX126x::SPIRead(uint8_t *txData, uint8_t txDataLen)
{
	cs_Pin(0);
	HAL_SPI_Receive_IT(spiI, txData, txDataLen);
	cs_Pin(1);
}

//----------------------------------------------------------------------------------------------------------------------------
//  The command...
//
//  Parameters:
//  none
//
//
//  Return value:
//  none
//  
//----------------------------------------------------------------------------------------------------------------------------
uint8_t SX126x::WriteBuffer(uint8_t *txData, uint8_t txDataLen)
{
    cs_Pin(0);
    uint8_t spiBuff[2];
    spiBuff[0]=SX126X_CMD_WRITE_BUFFER;
    spiBuff[1]=0;

    HAL_SPI_Transmit_IT(spiI, spiBuff, 2);
    HAL_SPI_Transmit_IT(spiI, txData, txDataLen);

    cs_Pin(1);
    while(busy_Pin());
 
    return 1;
}


void SX126x::SPIwriteCommand(uint8_t cmd, uint8_t* data, uint8_t numBytes, bool waitForBusy) {
  SPItransfer(cmd, true, data, NULL, numBytes, waitForBusy);
}


void SX126x::SPIreadCommand(uint8_t cmd, uint8_t* data, uint8_t numBytes, bool waitForBusy) {
  SPItransfer(cmd, false, NULL, data, numBytes, waitForBusy);
}


void SX126x::SPItransfer(uint8_t cmd, bool write, uint8_t* dataOut, uint8_t* dataIn, uint8_t numBytes, bool waitForBusy) {
  
  // ensure BUSY is low (state meachine ready)
  // TODO timeout
	 while(busy_Pin());

	  // start transfer
	  cs_Pin(0);

	  // send command byte
	  uint8_t cmd_t[1];
	  cmd_t[0]=cmd;
	  HAL_SPI_Transmit_IT(spiI, cmd_t,1);
	  // send/receive all bytes
	  if(write) {HAL_SPI_Transmit_IT(spiI, dataOut, numBytes); }

	  else {

	    HAL_SPI_Transmit_IT(spiI, SX126X_CMD_NOP, 1);
	    HAL_SPI_Receive_IT(spiI, dataIn, numBytes);

	  }
	  cs_Pin(1);

	  if(waitForBusy) {
	    HAL_Delay(1);
	    while(busy_Pin());
	  }
}
